package com.charlesriver.code.carr;

import com.charlesriver.code.carr.err.*;
import com.charlesriver.code.carr.dto.*;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface CarReservation {
    List<CarInventory> queryCarAvailability(
            Set<OffsetDateTime> reservationTimes
    ) throws DBErrorException, CarNoLongerAvailableException;

    List<UUID> createMultiDayReservation(OffsetDateTime[] fromDate,
                                         OffsetDateTime[] toDate,
                                         String carType,
                                         String carModel,
                                         String clientDetails,
                                         String clientPayment)
            throws DBErrorException, CarNoLongerAvailableException, PriceExpiredException, OverlappingDatesException;

    void deleteReservation(List<UUID> reservationId) throws DBErrorException;
}