package com.charlesriver.code.carr;


import com.charlesriver.code.carr.dto.*;
import com.charlesriver.code.carr.err.*;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class CarReservationBiz implements CarReservation {
    InventoryByTimeInterval carInventoryByTimeInterval;

    @Override
    public List<CarInventory> queryCarAvailability(Set<OffsetDateTime> reservationTimes) throws DBErrorException, CarNoLongerAvailableException {
        return carInventoryByTimeInterval.getAvailability(reservationTimes);
    }

    @Override
    public List<UUID> createMultiDayReservation(OffsetDateTime[] fromDate,
                                                OffsetDateTime[] toDate, String carType, String carModel, String clientDetails, String clientPayment)
            throws DBErrorException, PriceExpiredException, CarNoLongerAvailableException, OverlappingDatesException {

        if(overlappingTimes(fromDate, toDate)){
            throw new OverlappingDatesException();
        }

        Reservation reservation = new Reservation();
        reservation.setCarType(CarType.valueOf(carType));
        reservation.setCarModel(CarModel.valueOf(carModel));
        reservation.setClientDetails(clientDetails);
        reservation.setPaymentDetails(clientPayment);

        Set<OffsetDateTime> reservationDates = new LinkedHashSet<>();

        for(int i = 0; i < fromDate.length; i++){
            // TODO actually here I'd need to pass all the hours that fall within fromDate up to toDate
            reservationDates.add(fromDate[i].truncatedTo(ChronoUnit.HOURS));
            reservationDates.add(toDate[i].truncatedTo(ChronoUnit.HOURS));
        }

        return carInventoryByTimeInterval.makeReservation(reservationDates, reservation);
    }

    /**
     * Compares each toDate with the following fromDate, returning true if the times overlap.
     *
     * Assumes that fromDate and toDate have the same length and they come ordered as well.
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    boolean overlappingTimes(OffsetDateTime[] fromDate, OffsetDateTime[] toDate) {
        assert(fromDate != null && toDate != null && fromDate.length == toDate.length);

        for(int i = 0; i < fromDate.length; i++){
            if (i+1 < toDate.length){
                if(toDate[i].isAfter(fromDate[i+1])){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public void deleteReservation(List<UUID> reservationId) throws DBErrorException {
        carInventoryByTimeInterval.cancelReservations(reservationId);
    }
}
