package com.charlesriver.code.carr.dto;

import com.charlesriver.code.carr.err.CarNoLongerAvailableException;
import com.charlesriver.code.carr.err.DBErrorException;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Stores the car type inventory indexed by time interval (for example by hours, this is set by the caller of this class).
 *
 * Methods that modify the number of cars in the system are synchronized. Methods that show how many cars are available
 * aren't synchronized, so that overall system responsiveness is greater (at the risk of non serialized reads).
 */
public class InventoryByTimeInterval {
    private volatile Map<OffsetDateTime, CarInventory> inventoryByTime;

    public InventoryByTimeInterval(){
        inventoryByTime = new ConcurrentHashMap<>();
    }

    public List<CarInventory> getAvailability(Set<OffsetDateTime> reservationTimes) throws CarNoLongerAvailableException {
        List<CarInventory> carInventoryList = new ArrayList<>();

        for (OffsetDateTime reservationTime : reservationTimes) {
            CarInventory inventory = inventoryByTime.get(reservationTime);

            if(inventory == null){
                // no inventory for one of the requested times
                throw new CarNoLongerAvailableException();
            }else {
                carInventoryList.add(inventory);
            }
        }

        return carInventoryList;
    }

    public synchronized List<UUID> makeReservation(Set<OffsetDateTime> reservationTimes, Reservation reqReservation) throws CarNoLongerAvailableException, DBErrorException {
        List<UUID> reservationUUIDs = new ArrayList<>();

        // check that the cars are available for the whole interval, then reserve them
        try{
            getAvailability(reservationTimes);

            for (OffsetDateTime reservationTime : reservationTimes) {
                CarInventory inventory = inventoryByTime.get(reservationTime);

                reservationUUIDs.add(inventory.reserveCar(reqReservation.getCarType(), reqReservation));
            }
        }catch(CarNoLongerAvailableException | DBErrorException dbe){
            throw dbe;
        }

        return reservationUUIDs;
    }

    public synchronized void cancelReservations(List<UUID> reservationsByTime) throws DBErrorException {
        for(UUID reservationByTime : reservationsByTime) {
            for (CarInventory carInventory : inventoryByTime.values()) {
                try {
                    carInventory.cancelReservation(reservationByTime);
                } catch (DBErrorException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
    }

    public synchronized void updateInventoryForDate(OffsetDateTime dateTime, CarInventory inventory){
        CarInventory inventoryForDate = inventoryByTime.get(dateTime);

        if(inventoryForDate == null){
            inventoryByTime.put(dateTime, inventory);
        }
    }
}
