package com.charlesriver.code.carr.dto;

public enum CarType {
    SEDAN, CLASSIC, SUV, SPORT, COMPACT;
}
