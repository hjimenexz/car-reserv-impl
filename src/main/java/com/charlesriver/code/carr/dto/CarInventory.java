package com.charlesriver.code.carr.dto;

import com.charlesriver.code.carr.err.CarNoLongerAvailableException;
import com.charlesriver.code.carr.err.DBErrorException;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Inventory by car types for any given hour.
 *
 * Methods that modify the number of cars in the system are synchronized. Methods that show how many cars are available
 * aren't synchronized, so that overall system responsiveness is greater (at the risk of non serialized reads).
 */
public class CarInventory {
    private volatile Map<CarType, AtomicInteger> inventory;
    private volatile Map<UUID, Reservation> reservations;

    public CarInventory(){
        // Decorate Inventory Map with synchronized implementation
        inventory = new ConcurrentHashMap<>();
        reservations = new ConcurrentHashMap<>();
    }

    public synchronized UUID reserveCar(CarType carType, Reservation reservation) throws CarNoLongerAvailableException, DBErrorException {
        AtomicInteger storedInteger = inventory.get(carType);

        if(storedInteger != null){
            int availableCars = storedInteger.decrementAndGet();

            if(availableCars < 0){
                throw new CarNoLongerAvailableException();
            }

            UUID reservationUUID = UUID.randomUUID();
            reservations.put(reservationUUID, reservation);

            return reservationUUID;
        }else{
            throw new DBErrorException(); // requested carType not initialized.
        }
    }

    public synchronized void cancelReservation(UUID reservationId) throws DBErrorException {
        Reservation foundReservation = reservations.get(reservationId);

        if(foundReservation != null){
            AtomicInteger storedInteger = inventory.get(foundReservation.getCarType());

            if(storedInteger != null){
                // return the car to the inventory
                storedInteger.incrementAndGet();
            }else{
                // carType not initialized.
                throw new DBErrorException();
            }

            reservations.remove(reservationId);
        }
    }

    /**
     * Get method is not synchronized to put less contention on reads (getting the number of cars).
     *
     * @param carType
     * @return
     * @throws CarNoLongerAvailableException
     * @throws DBErrorException
     */
    public int getNumCars(CarType carType) throws CarNoLongerAvailableException, DBErrorException {
        AtomicInteger storedInteger = inventory.get(carType);

        if(storedInteger != null){
            return inventory.get(carType).get();
        }else{
            throw new DBErrorException(); // requested cathegory not initialized.
        }
    }

    public synchronized void initNumCars(CarType carType, Integer numCars){
        inventory.put(carType, new AtomicInteger(numCars));
    }

    public Set<CarType> getCarTypes(){
        return inventory.keySet();
    }
}
