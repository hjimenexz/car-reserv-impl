package com.charlesriver.code.carr.err;

public class OverlappingDatesException extends BusinessException {
    public OverlappingDatesException(){
        super(BusinessErrorCode.OVERLAPPING_DATES);
    }
}
