package com.charlesriver.code.carr.err;

public class DBErrorException extends BusinessException {
    public DBErrorException(){
        super(BusinessErrorCode.DB_ERROR);
    }
}
