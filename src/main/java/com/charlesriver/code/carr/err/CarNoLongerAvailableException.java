package com.charlesriver.code.carr.err;

public class CarNoLongerAvailableException extends BusinessException {
    public CarNoLongerAvailableException(){
        super(BusinessErrorCode.NO_CAR_AVAILABLE);
    }
}
