package com.charlesriver.code.carr.err;

public class PriceExpiredException extends BusinessException {
    public PriceExpiredException(){
        super(BusinessErrorCode.PRICE_EXPIRED);
    }
}
