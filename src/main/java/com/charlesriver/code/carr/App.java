package com.charlesriver.code.carr;

import com.charlesriver.code.carr.dto.CarInventory;
import com.charlesriver.code.carr.dto.CarType;
import com.charlesriver.code.carr.dto.InventoryByTimeInterval;
import com.charlesriver.code.carr.err.CarNoLongerAvailableException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        InventoryByTimeInterval sampleAppInventory = new InventoryByTimeInterval();

        OffsetDateTime initialTime = OffsetDateTime.now().truncatedTo(ChronoUnit.HOURS);

        for(int i = 0; i < 1000; i++) {
            CarInventory inventory = new CarInventory();
            inventory.initNumCars(CarType.SEDAN, 10);
            inventory.initNumCars(CarType.SUV, 20);
            inventory.initNumCars(CarType.SPORT, 30);
            inventory.initNumCars(CarType.COMPACT, 40);

            sampleAppInventory.updateInventoryForDate(initialTime.plusHours(i), inventory);
        }

        try {
            List<CarInventory> currentInventory = sampleAppInventory.getAvailability(Collections.singleton(initialTime));

            System.out.println("Current inventory is: " + ToStringBuilder.reflectionToString(currentInventory));

            currentInventory = sampleAppInventory.getAvailability(new LinkedHashSet<>(Arrays.asList(
                    new OffsetDateTime[]{initialTime, initialTime.plusHours(1), initialTime.plusHours(2)})));
            System.out.println("Current inventory is: " + ToStringBuilder.reflectionToString(currentInventory));

        } catch (CarNoLongerAvailableException e) {
            e.printStackTrace();
        }
    }
}
