package com.charlesriver.code.carr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.charlesriver.code.carr.dto.CarInventory;
import com.charlesriver.code.carr.dto.CarType;
import com.charlesriver.code.carr.dto.InventoryByTimeInterval;
import com.charlesriver.code.carr.dto.Reservation;
import com.charlesriver.code.carr.err.CarNoLongerAvailableException;
import com.charlesriver.code.carr.err.DBErrorException;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private InventoryByTimeInterval sampleAppInventory;
    private OffsetDateTime initialTime;

    @Before
    public void setUp() throws Exception {
        sampleAppInventory = new InventoryByTimeInterval();

        initialTime = OffsetDateTime.now().truncatedTo(ChronoUnit.HOURS);

        // initially setting up 10000 hours of inventory
        for(int i = 0; i < 10000; i++) {
            CarInventory inventory = new CarInventory();
            inventory.initNumCars(CarType.SEDAN, 10);
            inventory.initNumCars(CarType.SUV, 20);
            inventory.initNumCars(CarType.SPORT, 30);
            inventory.initNumCars(CarType.COMPACT, 40);

            sampleAppInventory.updateInventoryForDate(initialTime.plusHours(i), inventory);
        }
    }

    @Test
    public void checkAvailability() throws CarNoLongerAvailableException, DBErrorException {
        List<CarInventory> currentInventory = sampleAppInventory.getAvailability(Collections.singleton(initialTime));

        Assert.assertEquals(1, currentInventory.size());

        currentInventory = sampleAppInventory.getAvailability(new LinkedHashSet<>(Arrays.asList(
                new OffsetDateTime[]{initialTime, initialTime.plusHours(1), initialTime.plusHours(2)})));
        Assert.assertEquals(3, currentInventory.size());
        Assert.assertEquals(10, currentInventory.get(0).getNumCars(CarType.SEDAN));
    }

    @Test
    public void makeReservationForOneHour() throws CarNoLongerAvailableException, DBErrorException {
        Reservation reqReservation = new Reservation();
        reqReservation.setCarType(CarType.SEDAN);
        List<UUID> reservationUUIDs = sampleAppInventory.makeReservation(Collections.singleton(initialTime), reqReservation);

        List<CarInventory> updatedInventory = sampleAppInventory.getAvailability(Collections.singleton(initialTime));
        assertEquals("Inventory size should be 1 as only one hour was requested", 1,
                updatedInventory.size());
        assertEquals("There should be only 9 cars now! ", 9, updatedInventory.get(0).getNumCars(CarType.SEDAN));
    }

    @Test
    public void makeReservationForThreeHours() throws CarNoLongerAvailableException, DBErrorException{
        Reservation reqReservation = new Reservation();
        reqReservation.setCarType(CarType.SEDAN);

        Set<OffsetDateTime> reservationTimes = new LinkedHashSet<>(Arrays.asList(
                new OffsetDateTime[]{initialTime, initialTime.plusHours(1), initialTime.plusHours(2)}));
        List<UUID> reservationUUIDs = sampleAppInventory.makeReservation(reservationTimes, reqReservation);

        List<CarInventory> updatedInventory = sampleAppInventory.getAvailability(reservationTimes);
        assertEquals("Inventory size should be 3",
                updatedInventory.size(), 3);
        assertEquals("There should be only 9 cars now! ", 9, updatedInventory.get(0).getNumCars(CarType.SEDAN));
        // second hour
        assertEquals("There should be only 9 cars now! ", 9, updatedInventory.get(1).getNumCars(CarType.SEDAN));
        // third hour
        assertEquals("There should be only 9 cars now! ", 9, updatedInventory.get(2).getNumCars(CarType.SEDAN));
    }

    @Test
    public void cancelOneHourReservation() throws CarNoLongerAvailableException, DBErrorException{
        Reservation reqReservation = new Reservation();
        reqReservation.setCarType(CarType.SEDAN);
        List<UUID> reservationUUIDs = sampleAppInventory.makeReservation(Collections.singleton(initialTime), reqReservation);
        sampleAppInventory.cancelReservations(reservationUUIDs);

        List<CarInventory> updatedInventory = sampleAppInventory.getAvailability(Collections.singleton(initialTime));
        assertEquals("Inventory size should be 1 as only one hour was requested", 1,
                updatedInventory.size());
        assertEquals("There should 10 cars now! ", 10, updatedInventory.get(0).getNumCars(CarType.SEDAN));
    }

    @Test
    public void cancelMultipleHourReservation() throws CarNoLongerAvailableException, DBErrorException{
        Reservation reqReservation = new Reservation();
        reqReservation.setCarType(CarType.SEDAN);
        Set<OffsetDateTime> reservationTimes = new LinkedHashSet<>(Arrays.asList(
                new OffsetDateTime[]{initialTime, initialTime.plusHours(1), initialTime.plusHours(2)}));

        List<UUID> reservationUUIDs = sampleAppInventory.makeReservation(reservationTimes, reqReservation);
        sampleAppInventory.cancelReservations(reservationUUIDs);

        List<CarInventory> updatedInventory = sampleAppInventory.getAvailability(reservationTimes);
        assertEquals("Inventory size should be 3 as 3 hour was requested", 3,
                updatedInventory.size());
        assertEquals("There should 10 cars now! ", 10, updatedInventory.get(0).getNumCars(CarType.SEDAN));
        // second hour
        assertEquals("There should 10 cars now! ", 10, updatedInventory.get(1).getNumCars(CarType.SEDAN));
        // third hour
        assertEquals("There should 10 cars now! ", 10, updatedInventory.get(2).getNumCars(CarType.SEDAN));
    }
}
